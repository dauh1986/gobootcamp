package main

import (
	"fmt"
	"hour7/types"
)

func main() {
	a := types.Drink{
		Name: "Lemonade",
		Ice:  true,
	}
	b := &a
	b.Ice = false

	fmt.Printf("%+v\n", *b)
	fmt.Printf("%+v\n", a)
	fmt.Printf("%p\n", b)
	fmt.Printf("%p\n", &a)
}
