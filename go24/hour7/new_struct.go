package main

import (
	"fmt"
	"hour7/types"
)

func main() {
	m := new(types.Movie)
	m.Name = "Metropolis"
	m.Rating = 0.99
	fmt.Printf("%+v\n", m)
}
