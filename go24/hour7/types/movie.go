package types

type Movie struct {
	Name   string
	Rating float32
}
