package types

type Drink struct {
	Name string
	Ice  bool
}
