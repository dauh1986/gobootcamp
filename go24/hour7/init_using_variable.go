package main

import (
	"fmt"
	"hour7/types"
)

func main() {
	var m types.Movie
	fmt.Printf("%v\n", m)
	m.Name = "Metropolis"
	m.Rating = 0.9918
	fmt.Printf("%v\n", m)
}
