package main

import (
	"fmt"
	"hour7/types"
)

func main() {
	a := types.Drink{
		Name: "Lemonade",
		Ice:  true,
	}
	b := types.Drink{
		Name: "Lemonade",
		Ice:  true,
	}
	if a == b {
		fmt.Println("a and b are the same")
	}
	fmt.Printf("%+v\n", a)
	fmt.Printf("%+v\n", b)

}
