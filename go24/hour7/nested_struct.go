package main

import (
	"fmt"
	"hour7/types"
)

func main() {
	e := types.Superhero{
		Name: "Batman",
		Age:  32,
		Address: types.Address{
			Number: 1007,
			Street: "Mountain Drive",
			City:   "Gotham",
		},
	}
	fmt.Printf("%+v\n", e)
}
