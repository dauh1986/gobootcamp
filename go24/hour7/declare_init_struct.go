package main

import (
	"fmt"
	"hour7/types"
)

func main() {
	m := types.Movie{
		Name:   "Citizen Kane",
		Rating: 10,
	}
	fmt.Println(m.Name, m.Rating)
}
