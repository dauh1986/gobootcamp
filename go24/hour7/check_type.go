package main

import (
	"fmt"
	"hour7/types"
	"reflect"
)

func main() {
	a := types.Drink{
		Name: "Lemonade",
		Ice:  true,
	}
	b := types.Drink{
		Name: "Lemonade",
		Ice:  true,
	}

	fmt.Println(reflect.TypeOf(a))
	fmt.Println(reflect.TypeOf(b))
}
