package hour15

import "testing"

func TestGreeting(t *testing.T) {
	got := Greeting("World")
	want := "Hello World"

	if got != want {
		t.Fatalf("Expected %q, got %q", want, got)
	}
}

type GreetingTest struct {
	name   string
	locale string
	want   string
}

var greetingTests = []GreetingTest{
	{"George", "en-US", "Hello George"},
	{"Chloe", "fr-FR", "Bonjour Chloe"},
	{"Giuseppe", "it-IT", "Ciao Giuseppe"},
}

func TestGreetingLocale(t *testing.T) {
	for _, test := range greetingTests {
		got := GreetingLocale(test.name, test.locale)
		if got != test.want {
			t.Errorf("Greeting(%s,%s) =%v; want %v", test.name, test.locale, got, test.want)
		}
	}
}

func TestFrTranslation(t *testing.T) {
	got := Translate("fr-FR")
	want := "Bonjour "
	if got != want {
		t.Fatalf("Expected %q, got %q", want, got)
	}
}

func TestUSTranslation(t *testing.T) {
	got := GreetingLocale("George", "en-US")
	want := "Hello George"
	if got != want {
		t.Fatalf("Expected %q, got %q", want, got)
	}
}
