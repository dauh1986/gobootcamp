package hour15

func Greeting(s string) string {
	return ("Hello " + s)
}

func Translate(s string) string {
	switch s {
	case "en-US":
		return "Hello "
	case "fr-FR":
		return "Bonjour "
	case "it-IT":
		return "Ciao "
	default:
		return "Hello "
	}
}

func GreetingLocale(name string, locale string) string {
	salutation := Translate(locale)
	return salutation + name
}
