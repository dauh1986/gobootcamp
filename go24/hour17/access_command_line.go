package main

import (
	"fmt"
	"os"
)

func main() {
	for i, arg := range os.Args {
		fmt.Println("argument", i, "is", arg)
	}
}

//usage go run .\access_command_line.go "a" "b" "c"
