package main

import (
	"fmt"
)

func main() {
	var cheeses = make([]string, 3)
	cheeses[0] = "Mariolles"
	cheeses[1] = "Epoisses de Bourgogne"
	cheeses[2] = "Cammembert"
	fmt.Println(len(cheeses))
	fmt.Println(cheeses)
	fmt.Println("---------------------")
	fmt.Println(cheeses[:1])
	fmt.Println(cheeses[:2])
	fmt.Println(cheeses[:2+1])
	fmt.Println("---------------------")
	fmt.Println(cheeses[1:])
	fmt.Println(cheeses[2:])
	fmt.Println(cheeses[2+1:])
	fmt.Println("---------------------")
	cheeses = append(cheeses[:2], cheeses[2+1:]...)
	fmt.Println(len(cheeses))
	fmt.Println(cheeses)
}
