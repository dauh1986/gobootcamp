package main

import (
	"fmt"
	"time"
)

func slowFunc2(c chan string) {
	for msg := range c {
		fmt.Println(msg)
	}
}

func main() {
	messages := make(chan string, 2)
	messages <- "hello"
	messages <- "world"

	close(messages)

	fmt.Println("Pushed two messages onto channel with no receive")
	time.Sleep(time.Second * 1)
	slowFunc2(messages)
}
