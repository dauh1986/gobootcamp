package main

import (
	"fmt"
)

func showMemoryAddressPointer(x *int) {
	fmt.Println(x)
	return
}

func main() {
	i := 1
	fmt.Println(&i)
	showMemoryAddressPointer(&i)
}
