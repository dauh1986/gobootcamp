package main

import "fmt"

func main() {

	var s string
	s = "Hello World"
	fmt.Println(s)

	//shorthand
	var a, b string = "foo", "bar"
	fmt.Println(a)
	fmt.Println(b)

	//shorthand different types
	var (
		x string = "foo"
		y int    = 4
	)
	fmt.Println(x)
	fmt.Println(y)

	var i int
	var f float32
	var b2 bool
	var str2 string
	fmt.Printf("%v %v %v %q\n", i, f, b2, str2)

}
