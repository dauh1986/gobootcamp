PACKAGE DOCUMENTATION

package example03
import "./example03"

    Package example03 shows how to use the godoc tool.

VARIABLES

var ErrNotAnAnimal = errors.New("Name is not an animal")
ErrNotAnAnimal is returned if the name field of the Animal struct is
Human.

TYPES

type Animal struct {
Name string // Name holds the name of an Animal.
// Age holds the name of an Animal.
Age int
}
Animal specifies an animal

func (a Animal) Hello() (string, error)

1:  all: check-gofmt
2:
3:  check-gofmt:
4:  @if [ -n "$(shell gofmt -l .)" ]; then \
5:      echo 1>&2 'The following files need to be formatted:'; \
6:      gofmt -l .; \
7:      exit 1; \
8:  fi