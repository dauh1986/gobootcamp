package main

import (
	"fmt"
)

func main() {
	i := 4
	if i == 3 {
		fmt.Println("i is 3")
	} else if i == 2 {
		fmt.Println("i is 2")
	} else {
		fmt.Println("i is smth else")
	}
}
