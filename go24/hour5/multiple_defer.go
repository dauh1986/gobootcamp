package main

import (
	"fmt"
)

func main() {
	defer fmt.Println("I'm the first defer statement")
	defer fmt.Println("I'm the second defer statement")
	defer fmt.Println("I'm the third defer statement")
	fmt.Println("Hello world!")
}
