package main

import (
	"hour8/types"
)

func main() {
	t := types.T850{
		Name: "The Terminator",
	}

	r := types.R2D2{
		Broken: true,
	}

	types.Display(&t)
	types.Display(&r)
}
