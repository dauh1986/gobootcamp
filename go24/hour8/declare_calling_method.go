package main

import (
	"fmt"
	"hour8/types"
)

func main() {
	m := types.Movie{
		Name:   "Spiderman",
		Rating: 3.2,
	}
	fmt.Println(m.Summary())
}
