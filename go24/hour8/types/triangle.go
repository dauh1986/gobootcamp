package types

type Triangle struct {
	Base   float64
	Height float64
}

func (t *Triangle) Area() float64 {
	return 0.5 * (t.Base * t.Height)
}

func (t Triangle) ChangeBaseValue(f float64) {
	t.Base = f
	return
}

func (t *Triangle) ChangeBaseRef(f float64) {
	t.Base = f
	return
}
