package types

import "strconv"

type Movie struct {
	Name   string
	Rating float64
}

func (m *Movie) Summary() string {
	r := strconv.FormatFloat(m.Rating, 'f', 1, 64)
	return m.Name + ", " + r + " yupii"
}
