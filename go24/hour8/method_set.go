package main

import (
	"fmt"
	"hour8/types"
)

func main() {
	s := types.Sphere{
		Radius: 5,
	}

	fmt.Println(s.SurfaceArea())
	fmt.Println(s.Volume())
}
