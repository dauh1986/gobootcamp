package main

import (
	"fmt"
	"hour8/types"
)

func main() {
	t := types.Triangle{
		Base:   3,
		Height: 1,
	}
	t.ChangeBaseValue(4)
	fmt.Println(t.Base)
}
